import sys, pygame, os
import datetime
pygame.init()

size = width, height = 1366, 768
negro = 0, 0, 0
blanco = 250, 250, 250
plomo = 20, 20, 20
verde = 0, 250, 0
rojo = 255, 0, 0
amarillo = 255, 233, 0
tam_font_mayor = 200
tam_font_menor = 150

reloj = pygame.time.Clock()

val_txt_local = "LOCAL"
val_txt_visita = "VISITA"
val_local = 0
val_visita = 0
val_falta_local = 0
val_falta_visita = 0
val_minutos = 0
val_segundos = 0
val_periodo = 0
val_tiempo = False

font = pygame.font.Font(os.path.join("DJBGetDigital.ttf"), tam_font_mayor)
font_88 = pygame.font.Font(os.path.join("DJBGetDigital.ttf"), tam_font_menor)
font_default = pygame.font.Font(None, 36)

#screen = pygame.display.set_mode(size, flags=pygame.FULLSCREEN)
screen = pygame.display.set_mode(size)

background = pygame.Surface(screen.get_size())
background = background.convert()
background.fill(negro)

#txt_local_pos.centerx = background.get_rect().centerx

def dibujar(background, val_local, val_visita, val_falta_local, val_falta_visita, val_minutos, val_segundos, val_periodo):
	espacio_1 = int((tam_font_mayor / 2) * 0.61)
	espacio_2 = int((tam_font_menor / 2) * 0.61)
	marcador_fondo = font.render("888",True, plomo)
	marcador_local = font.render(str(val_local),True, verde)
	marcador_visita = font.render(str(val_visita),True, verde)
	marcador_fondo_88 = font_88.render("88",True, plomo)
	falta_local = font_88.render(str(val_falta_local),True, rojo)
	falta_visita = font_88.render(str(val_falta_visita),True, rojo)
	tiempo_fondo = font.render("88",True, plomo)
	minutos = font.render(str(val_minutos),True, amarillo)
	segundos = font.render(str(val_segundos),True, amarillo)
	separador = font.render(":",True, amarillo)
	periodo_fondo = font_88.render("0",True, plomo)
	periodo = font_88.render(str(val_periodo),True, amarillo)

	fila_1 = 70
	txt_local = font_default.render(val_txt_local, 1, blanco)
	background.blit(txt_local, ((width/4) - (txt_local.get_rect().width / 2), fila_1 - 20))
	txt_visita = font_default.render(val_txt_visita, 1, blanco)
	background.blit(txt_visita, (((width/4) * 3) - (txt_visita.get_rect().width / 2), fila_1 - 20))
	width_marcador = marcador_fondo.get_rect().width / 2
	background.blit(marcador_fondo, ((width/4) - width_marcador, fila_1))
	background.blit(marcador_fondo, (((width/4) * 3) - width_marcador, fila_1))
	tam_local = (width/4) - width_marcador
	if (str(val_local))[0] == "1":
		background.blit(marcador_local, (tam_local + espacio_1, fila_1))
	else:
		background.blit(marcador_local, (tam_local, fila_1))
	tam_visita = ((width/4) * 3) - width_marcador
	if (str(val_visita))[0] == "1":
		background.blit(marcador_visita, (tam_visita + espacio_1, fila_1))
	else:
		background.blit(marcador_visita, (tam_visita, fila_1))

	fila_2 = 330
	txt_falta_local = font_default.render("FALTA", 1, blanco)
	background.blit(txt_falta_local, ((width/4) - (txt_falta_local.get_rect().width / 2), fila_2 - 20))
	background.blit(txt_falta_local, (((width/4) * 3) - (txt_falta_local.get_rect().width / 2),fila_2 - 20))
	width_marcador_88 = marcador_fondo_88.get_rect().width / 2
	background.blit(marcador_fondo_88, ((width/4) - width_marcador_88, fila_2))
	background.blit(marcador_fondo_88, (((width/4) * 3) - width_marcador_88, fila_2))
	background.blit(falta_local, ((width/4) - width_marcador_88, fila_2))
	background.blit(falta_visita, (((width/4) * 3) - width_marcador_88, fila_2))

	txt_periodo = font_default.render("PERIODO", 1, blanco)
	background.blit(txt_periodo, ((width / 2) - (txt_periodo.get_rect().width / 2), fila_2 - 20))
	width_periodo = periodo_fondo.get_rect().width / 2
	background.blit(periodo_fondo, ((width/2) - width_periodo, fila_2))
	background.blit(periodo, ((width/2) - width_periodo, fila_2))

	fila_3 = 540
	txt_tiempo = font_default.render("TIEMPO", 1, blanco)
	background.blit(txt_tiempo, ((width / 2) - (txt_tiempo.get_rect().width / 2), fila_3 - 20))
	tamanio = (tiempo_fondo.get_rect().width * 2) + separador.get_rect().width
	tamanio_minutos = (width/2)-(tamanio/2)
	background.blit(tiempo_fondo, (tamanio_minutos, fila_3))
	background.blit(minutos, (tamanio_minutos, fila_3))
	background.blit(separador, (tamanio_minutos + tiempo_fondo.get_rect().width, fila_3))
	background.blit(tiempo_fondo, (tamanio_minutos + tiempo_fondo.get_rect().width + separador.get_rect().width, fila_3))
	background.blit(segundos, (tamanio_minutos + tiempo_fondo.get_rect().width + separador.get_rect().width, fila_3))
tiempo_so = datetime.datetime.now()
while 1:
	for event in pygame.event.get():
		if event.type == pygame.QUIT: sys.exit()
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_1 or event.key == pygame.K_KP1 or event.key == pygame.K_q:
				val_local += 1
			if event.key == pygame.K_2 or event.key == pygame.K_KP2 or event.key == pygame.K_a:
				val_local -= 1
			if event.key == pygame.K_3 or event.key == pygame.K_KP3 or event.key == pygame.K_w:
				val_falta_local += 1
			if event.key == pygame.K_s:
				val_falta_local -= 1
			if event.key == pygame.K_4 or event.key == pygame.K_KP4 or event.key == pygame.K_y:
				val_visita += 1
			if event.key == pygame.K_5 or event.key == pygame.K_KP5 or event.key == pygame.K_h:
				val_visita -= 1
			if event.key == pygame.K_6 or event.key == pygame.K_KP6 or event.key == pygame.K_u:
				val_falta_visita += 1
			if event.key == pygame.K_j:
				val_falta_visita -= 1
			if event.key == pygame.K_p:
				val_periodo += 1
			if event.key == pygame.K_o:
				val_periodo -= 1
			if event.key == pygame.K_RETURN:
				val_tiempo = not val_tiempo
			if event.key == pygame.K_r:
				val_segundos = 0
				val_minutos = 0
			if event.key == pygame.K_ESCAPE:
				sys.exit()

	if val_tiempo:
		if val_segundos == 60:
			val_segundos = 0
			val_minutos += 1
		now = datetime.datetime.now()
		if (now.minute, now.second) != (tiempo_so.minute, tiempo_so.second):
			tiempo_so = now
			val_segundos += 1
		#limpia la pantalla
	screen.fill(negro)
	#actualiza pantalla con los nuevos datos
	#screen.blit(background, (0, 0))
	dibujar(screen, val_local, val_visita, val_falta_local, val_falta_visita, val_minutos, val_segundos, val_periodo)

	print(pygame.time.get_ticks()/1000)
	#################################
	pygame.display.flip()
	reloj.tick(100)
	#pygame.time.delay(1000)
pygame.quit()

